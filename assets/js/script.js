const notANumber = () => {
	const input = prompt('give me a value');
	Number(input);
	isNaN(input) ? console.log('not a number') : console.log('its a number');
};

const leapYear = () => {
	const input = prompt('give a year');
	Number(input);
	if (isNaN(input)) {
		console.log('please input a valid year');
	} else if (input % 4 == 0) {
		console.log('it is a leap year');
	} else if (input % 4 !== 0) {
		console.log('it is not a leap year');
	}
};

const aveGrade = () => {
	const grade = [];
	for (let i = 0; i < 5; i++) {
		grade.push(Number(prompt('give a grade')));
	}
	const ave = grade.reduce((a, b) => a + b, 0) / 5;
	if (ave < 101 && ave > 62) {
		ave > 74 ? console.log('passed') : console.log('failed');
	} else {
		console.log('please input a valid grade');
	}
};
